package weather.models;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenWeather {

	private WeatherLogTemp weatherLogTemp;

	private String name;
	private List<Weather> weather;
	private Main main;

	public OpenWeather() {

	}

	public OpenWeather(String name, List<Weather> weather, Main main) {
		this.name = name;
		this.weather = weather;
		this.main = main;
	}

	public WeatherLogTemp createJsonResponse() {
		weatherLogTemp = new WeatherLogTemp();
		weatherLogTemp.setResponseId(UUID.randomUUID().toString());
		weatherLogTemp.setLocation(name);
		weatherLogTemp.setActualWeather(weather.get(0).description);
		weatherLogTemp.setTemperature(main.temp);
		return weatherLogTemp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Weather> getWeather() {
		return weather;
	}

	public void setWeather(List<Weather> weather) {
		this.weather = weather;
	}

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

	static class Weather {

		private String description;

		public Weather() {

		}

		public Weather(String description) {
			this.description = description;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
	}

	static class Main {

		private String temp;

		public Main() {

		}

		public Main(String temp) {
			this.temp = temp;
		}

		public String getTemp() {
			return temp;
		}

		public void setTemp(String temp) {
			this.temp = temp;
		}
	}
}
