package weather.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import weather.models.WeatherLogTemp;
import weather.services.WeatherLogService;

@RestController
@RequestMapping(value="/weatherlog")
public class WeatherLogController {

	@Autowired
	private WeatherLogService weatherLogService;
	
	/**
	 * API #1
	 * 
	 * API that displays list of weather information from London, Prague, and San Francisco. 
	 * Information should include location, actual weather, and temperature.
	 * Response should be in JSON format.
	 * 
	 * GET http://localhost:8080/weatherlog?location={cityName}
	 * 
	 * or
	 * 
	 * GET http://localhost:8080/weatherlog (defaults to London)
	 * 
	 * @param location
	 * @return JSON consisting of location, actualWeather, and temperature of location specified.
	 */
	@GetMapping
	public WeatherLogTemp getWeatherLogByLocation(@RequestParam(value="location", defaultValue="London") String location) {
		return  weatherLogService.getWeatherLogByLocation(location);
	}
	
	
	/**
	 * API #2
	 * 
	 * API that store last five unique responses of API #1. 
	 * Information should be saved in a database.
	 * Table Name: Weatherlog
	 * Fields:
	 *  id (unique auto number) 	long
	 *  responseId (unique guid) 	String
	 *  location 					String
	 *  actualWeather				String
	 *  temperature					String
	 *  dtimeInserted				timestamp
	 *  
	 *  POST http://localhost:8080/weatherlog
	 *  
	 */
	@PostMapping
	public void saveWeatherLog() {
		weatherLogService.saveWeatherLog();
	}

}
