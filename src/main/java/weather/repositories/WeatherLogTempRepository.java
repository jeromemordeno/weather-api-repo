package weather.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import weather.models.WeatherLogTemp;

public interface WeatherLogTempRepository extends JpaRepository<WeatherLogTemp, Long> {

	List<WeatherLogTemp> findTop5ByOrderByIdDesc();
}
