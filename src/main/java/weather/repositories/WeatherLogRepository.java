package weather.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import weather.models.WeatherLog;

public interface WeatherLogRepository extends JpaRepository<WeatherLog, Long> {
}
