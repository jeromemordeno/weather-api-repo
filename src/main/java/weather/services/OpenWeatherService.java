package weather.services;

import weather.models.WeatherLogTemp;

public interface OpenWeatherService {
	WeatherLogTemp getWeatherLogByLocation(String location);
}
