package weather.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import weather.models.WeatherLog;
import weather.models.WeatherLogTemp;
import weather.repositories.WeatherLogRepository;
import weather.repositories.WeatherLogTempRepository;

@Service
public class WeatherLogServiceImpl implements WeatherLogService {

	@Autowired
	private OpenWeatherService openWeatherService;
	
	@Autowired
	private WeatherLogRepository weatherLogRepository;
	
	@Autowired
	private WeatherLogTempRepository weatherLogTempRepository;
	
	private WeatherLog weatherLog = null;
	
	private WeatherLogTemp weatherLogTemp = null;
	
	@Override
	public WeatherLogTemp getWeatherLogByLocation(String location) {
		weatherLogTemp = openWeatherService.getWeatherLogByLocation(location);
		if(weatherLogTemp != null) {
			saveWeatherLogTemp(weatherLogTemp);
		}
		return weatherLogTemp;
	}
	
	public void saveWeatherLog() {
		List<WeatherLog> weatherLogList = new ArrayList<>();
		getWeatherLogTempRecord().stream()
		.forEach(weatherLogTemp -> {
			weatherLog = new WeatherLog();
			weatherLog.setResponseId(weatherLogTemp.getResponseId());
			weatherLog.setLocation(weatherLogTemp.getLocation());
			weatherLog.setActualWeather(weatherLogTemp.getActualWeather());
			weatherLog.setTemperature(weatherLogTemp.getTemperature());
			weatherLogList.add(weatherLog);
		});
		
		weatherLogRepository.saveAll(weatherLogList);
		weatherLogTempRepository.deleteAll();
	}
	
	private void saveWeatherLogTemp(WeatherLogTemp weatherLog) {
		weatherLogTempRepository.save(weatherLog);
	}
	
	private List<WeatherLogTemp> getWeatherLogTempRecord() {
		return weatherLogTempRepository.findTop5ByOrderByIdDesc();
	}
}
