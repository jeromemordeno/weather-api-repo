package weather.services;

import weather.models.WeatherLogTemp;

public interface WeatherLogService {
	WeatherLogTemp getWeatherLogByLocation(String location);
	
	void saveWeatherLog();
}
