package weather.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import weather.models.OpenWeather;
import weather.models.WeatherLogTemp;

@Service
public class OpenWeatherServiceImpl implements OpenWeatherService {
	
	@Value("${openweather.url}")
	private String apiUrl;

	@Value("${openweather.appid}")
	private String apiAppid;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public WeatherLogTemp getWeatherLogByLocation(String location) {
		return restTemplate.getForEntity(UriComponentsBuilder
				.fromHttpUrl(apiUrl)
				.path("/weather")
				.queryParam("q", location)
				.queryParam("appid", apiAppid).build().toUriString(), OpenWeather.class)
				.getBody()
				.createJsonResponse();
	}
}
