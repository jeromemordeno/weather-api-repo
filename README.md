# weather-api-repo
Homework:
Weather API that returns and stores weather information on locations.

API #1
GET http://localhost:8080/weatherlog?location={cityName}

API #2
POST http://localhost:8080/weatherlog

Database: H2 in-memory

H2 console:
http://localhost:8080/h2